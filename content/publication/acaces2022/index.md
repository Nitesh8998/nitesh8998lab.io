+++
title = "Characterizing DNN Workloads on General Purpose CPUs"
authors = ["**_N.N GS_**","F. Silfa","A. González"]
date = "2022-06-11"
tags = []
publication_types = ["2"]
publication = "_ACACES 2022: Poster Abstracts_"
publication_short = ""
summary = ""
featured = false
projects = []
slides = ""
url_pdf = "https://scholar.google.com/citations?view_op=view_citation&hl=en&user=xcUuBy0AAAAJ&citation_for_view=xcUuBy0AAAAJ:u5HHmVD_uO8C"
url_code = ""
url_dataset = ""
url_poster = "/publication/acaces2022/hipeacPoster-Nitesh.pdf"
url_slides = ""
url_source = ""
url_video = ""
math = true
highlight = true
[image]
image = ""
caption = ""
+++

