---
# Display name
name: G S Nitesh Narayana

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Researcher

# Organizations/Affiliations
organizations:
- name: CAIRN/TARAN - IRISA INRIA, AMD Bangalore, IIT Madras, IIITDM Kancheepuram 
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: The moment I realised the difference between 'diving' into the problem and 'drowning' in the problem is when I became a researcher. 

interests:
- Parallel Computer Systems Design and Architecture
- Memory Systems
- Quantum Computing
- Design Thinking

education:
  courses:
  - course: B.Tech + M.Tech in Computer Engineering
    institution: Indian Institute of Information Technology Design and Manufacturing Kancheepuram
    year: 2016-21
  - course: School Education 
    institution: Delhi Public School Hyderabad 
    year: 2003-16

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/nitesh-narayana-gs/
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/Nitesh8998
- icon: github
  icon_pack: fab
  link: https://github.com/Nitesh8998
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
- icon: cv
  icon_pack: ai
  link: files/cv_Nitesh.pdf



# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

Hola! Konnichiwa! 

I am a bicycle adventurer, sports and anime geek, accidental poet, passive artist, balcony
singer, emergency photographer, design thinker...

I am also an insanely passionate researcher and like to venture into problems related to Memory Systems, Computer Architecture, Parallel Computer Architecture.
Typically anything that is in the range of NAND-to-Kernel interests me (Building myself on the above the kernel part!!). 


Teaching and Thinking are two of my favourite hobbies. 
I also like exploring new and crazy things :sunglasses::metal:


If you are as crazy as me for new and exciting things and want to collaborate, lets have a chat!
 or you can write to my mentors as well!

Some of my own quotes: 
- "With Great Talent comes Greater Expectations"
- "Never join the dots that aren't even on the same side of the page"
- "Life is Complex, its upto you to not make it *Complicated*"
- "Be the Hero of the Actor-Network of your life"
- "Never compare a SpaceShip and a Submarine, they have their own 'pressures' and 'densities' to deal
  with"
- "If you can't fight for 'it', you have no right to complain about 'it' either."
- "Amidst all the life complications only the child in us keeps us sane and crazy :P"
- others in [Crazy_Words](https://nitesh.contact/talk/crazy_words/)


I have been mentored by the following brilliant minds:
- [Prof. Olivier Sentieys](http://people.rennes.inria.fr/Olivier.Sentieys/) and [Prof.
  Angeliki Kritikakou](https://sites.google.com/site/angelikikritikakou/)
  - They are have research engineer positions open for FPGA Accelerators
- [Prof. Sudhir Varadharajan, IIITDM Kancheepuram](https://www.linkedin.com/in/sudhirvaradarajan/)
  - He is currently looking for design engineers to work on innovative projects and product ideas.
- [Dr. Debiprasanna Sahoo, AMD Bangalore](https://sites.google.com/view/debiprasannasahoo/home)
  - He will join IIT Roorkee as an Assistant Professor and is in search of students who are willing to pursue Masters or PhD
    in the area of Computer Systems.
- [Prof. Madhu Mutyam, IIT Madras](http://www.cse.iitm.ac.in/~madhu/)
  - He currently has open PhD positions  and is looking for students who are passionate about Computer Architecture.
- [Dr. Patanjali SLPSK, University of Florida](https://patanjali.github.io/)
  - He is always open to work with people interested in Hardware Security.
- [Dr. S R Swamy (Surya) Saranam, Xilinx Hyderabad](https://www.linkedin.com/in/srswamy/)
  - He is open for collaboration in areas related to FPGAs and Computer Architecture
- [Dr. Noor Mahammad Sk.](http://web.iiitdm.ac.in/noor/)
- [Lavanya J](https://www.linkedin.com/in/lavanya-jagadeeswaran/)
  - She is actively looking for verification engineers for Vyoma Systems



