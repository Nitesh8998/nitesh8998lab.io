+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
  title = "Research Intern, CAIRN/TARAN "
  company = "IRISA INRIA"
  company_url = ""
  location = "Rennes, France"
  date_start = "2021-06-01"
  date_end = "2021-08-31"
  description = """
  Design Space Exploration for LSTM accelerator design using Vitis Toolchain
  
  """



[[experience]]
  title = "Co-Op Engineer, Core Performance Team"
  company = "AMD Pvt Ltd"
  company_url = ""
  location = "Bangalore, India"
  date_start = "2020-08-03"
  date_end = "2021-07-16"
  description = """
  Working on devoloping ML based simulation framework.
  """


[[experience]]
  title = "Researcher"
  company = ""
  company_url = ""
  #company_logo = 
  location = "IIT Madras"
  date_start = "2019-07-15"
  date_end = "2021-05-31"
  description = """
 

* Worked on devoloping Value Prediction Techniques for improving processor performance and Hardware Prefecthing in SSDs.  
* Worked on FPGAs, DRAMS and Deep Learning Accelerators. 

  """

[[experience]]
  title = "Teaching Assistant"
  company = ""
  company_url = ""
  location = "IIT Madras"
  date_start = "2019-08-01"
  date_end = "2020-12-21"
  description = """
  Handling the Advance Computer Organisation and Architecture course(Fall 2019, 2020). Under the guidance of Prof. Madhu Mutyam.
  """

[[experience]]
  title = "Project Intern"
  company = "Shakti Team - RISE Labs"
  company_url = ""
  location = "IIT Madras"
  date_start = "2019-05-13"
  date_end = "2019-07-13"
  description = """
  As a part of the verification team, Designed DUT interface with simulator API for SHAKTI Core verification.
  """

[[experience]]

  title = "Research Fellow and Lab Incharge(HPC)"
  company = "HPRCSE Lab"
  company_url = ""
  location = "IIITDM Kancheepuram"
  date_start = "2018-12-01"
  date_end = "2020-06-01"
  description = """
  
     
  * Dealt with research problems related to computer architecture  and network systems. 
  * Handled projects related to HPC.
  * Gave talks on Code Profiling and CUDA programming at anual HPC workshops by lab.
  """


#[[experience]]
#  title = "Project Intern"
#  company = "CavenTek"
#  company_url = ""
#  location = "MadeIT IIITDM"
#  date_start = "2016-11-01"
#  date_end = "2016-12-31"
#  description = """Worked on Physical File Tracking System. Developed the UI for it."""
#
+++
